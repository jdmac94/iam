package tests;

import static org.junit.Assert.*;

import org.iesam.calculadora.Calculadora;
import org.iesam.calculadora.ZeroException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Testcalculadora {
	
	private Calculadora calculadora;
	private int total=0;
	private int num1=10, num2=0, num3=5;
	
	
	@Before	
	public void setUp() {
		calculadora = new Calculadora();
	}

	@Test
	public void test_multiplica() {
				
		total = calculadora.multiplicar(num1, num2);
		assertEquals(num1*num2, total);

	}
	
	@Test
	public void test_resta() {
				
		total = calculadora.restar(num1, num2);
		assertEquals(num1-num2, total);
	}

	@Test
	public void test_sumar() {
				
		total = calculadora.sumar(num1, num2);
		assertEquals(num1+num2, total);
	}
	
	@Test
	public void test_dividir() throws ZeroException {
				
		total = calculadora.dividir(num1, num3);
		assertEquals(num1/num3, total);
	}
	
	@Test(expected = ZeroException.class)
	public void testExceptionZero() throws ZeroException {
	   
	    	total = calculadora.dividir(num1, num2);
	        fail();
	   }	
	
	
	@After
	public void tearDown() {
		calculadora = null;
	}

}
