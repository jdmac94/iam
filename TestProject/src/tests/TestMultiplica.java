package tests;
import static org.junit.jupiter.api.Assertions.*;

import act1uf2.Multiplica;

class TestMultiplica {

	
	@org.junit.jupiter.api.Test
	public void test_multiplica() {
		Multiplica multi = new Multiplica();
		
		int total = multi.multiplicador(3, 4);
		assertEquals(3*4, total);

	}


}
