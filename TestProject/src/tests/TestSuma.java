package tests;
import static org.junit.jupiter.api.Assertions.*;

import act1uf2.Suma;

class TestSuma {


	@org.junit.jupiter.api.Test
	public void test_suma() {
		Suma suma = new Suma();
		
		int total = suma.sumatori(3, 4);
		assertEquals(3+4, total);
	}
}
