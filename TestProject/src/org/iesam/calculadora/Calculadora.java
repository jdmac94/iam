package org.iesam.calculadora;

public class Calculadora {    

	  public int sumar(int operando1, int operando2) {
	     return operando1 + operando2;
	  }

	  public int restar(int operando1, int operando2) {
	     return operando1 - operando2;
	  }

	  public int multiplicar(int operando1, int operando2) {
	     return operando1 * operando2;
	  }      

	  public int dividir(int operando1, int operando2) throws ZeroException {
	     
		  if(operando2==0) {
				 throw new ZeroException();
		  }

		  
			 return operando1 / operando2;
		 
		
	  }    
}